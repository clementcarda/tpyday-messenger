#! ./venv/bin/python
import socket
import argparse
import threading
import pickle
import select

parser = argparse.ArgumentParser()
parser.add_argument("-H", "--host", help="configure listening IP. Default is 127.0.0.1", default="127.0.0.1")
parser.add_argument("-P", "--port", help="configure listening Port. Default is 65432", default=65432)
args = parser.parse_args()


class ClientThread(threading.Thread):

    def __init__(self, ip, port, clientsocket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.clientsocket = clientsocket
        print("[+] Nouvelle connexion de {} via port {}. Création d'un thread".format(self.ip, self.port))

    def run(self):
        while True:
            self.r = self.clientsocket.recv(1024)
            self.answ = pickle.loads(self.r)

            if isinstance(self.answ, dict):
                #TODO transfert message to right dest
                print("message sent by {}@{} to {} \ncontent : {}".format(self.answ["send"], self.ip, self.answ["dest"], self.answ["msg"]))
            elif isinstance(self.answ, str) and self.answ != "exit":
                Clients.append({"user": self.answ, "ip": self.ip, "port": self.port})
                print(Clients)
            elif isinstance(self.answ, str) and self.answ == "exit":
                # TODO clear disconection from client and deletion from Clients list
                pass

class ConnToClient(threading.Thread):
    def __init__(self, ip, port, msg):
        threading.Thread.__init__(self)
        self.cliIP = ip
        self.cliPort = port
        self.msg = msg
    def run(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ip, port))


def removeClient(user):
    for c in range(0, len(Clients)+1):
        if c["user"] == user:
            Clients.remove(c)


def TransfertMessage(socket, message, sender):
    #TODO Send message to right dest
    dest = GetUserInfo(message["dest"])
    pass

def GetUserInfo(user):
    dest = None
    for c in Clients:
        if c["user"] == user:
            dest = c
            break
    return dest

if __name__ == '__main__':



    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((args.host, args.port))

    Clients = []

    print("en écoute")
    while True:

        sock.listen()
        (clientsocket, (ip, port)) = sock.accept()
        nthread = ClientThread(ip, port, clientsocket)
        nthread.start()